//
//  LentaController.swift
//  lesson1Mouth6
//
//  Created by Rashit Osmonov on 6/5/22.
//

import Foundation
import UIKit
import SnapKit

class LentaCell: UITableViewCell{

    private lazy var publicationImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "1")
        return view
    }()
    private lazy var name: UILabel = {
        let view = UILabel()
        view.text = "Курманжан Датка"
        return view
    }()
    private lazy var star: UILabel = {
        let view = UILabel()
        return view
    }()
    private lazy var title: UILabel = {
        let view = UILabel()
        view.text = "Курманжан Датка выфвфывфывфывфывфывфывфы"
        view.numberOfLines = 10
        view.textAlignment = .left
        return view
    }()
    override func layoutSubviews() {
        addSubview(publicationImage)
        publicationImage.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(20)
            make.width.equalTo(100)
            make.height.equalTo(200)
            make.bottom.equalToSuperview()
        }
        addSubview(name)
        name.snp.makeConstraints { make in
            make.left.equalTo(publicationImage.snp.right).offset(30)
            make.top.equalToSuperview().offset(50)
        }
        addSubview(star)
        star.snp.makeConstraints { make in
            make.top.equalTo(name.snp.bottom)
            make.left.equalTo(publicationImage.snp.right).offset(65)
        }
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalTo(star.snp.bottom).offset(5)
            make.left.equalTo(publicationImage.snp.right).offset(20)
            make.height.equalTo(100)
            make.width.equalTo(190)
        }
    }
    func fill(model: MainModel){
        self.publicationImage.image = .init(named: model.image!)
        self.name.text = model.name!
        self.star.text = model.star?.description
        self.title.text = model.title!
    }
}
