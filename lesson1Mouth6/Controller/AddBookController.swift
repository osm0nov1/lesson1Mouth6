//
//  AddBookController.swift
//  lesson1Mouth6
//
//  Created by Rashit Osmonov on 12/5/22.
//

import Foundation
class AddBookController {
    func addBooks(model: MainModel){
        let book = MainModel(image: model.image, name: model.name, star: model.star, title: model.title)
        LocalStore.shared.books.append(book)
    }
}
