//
//  ViewController.swift
//  lesson1Mouth6
//
//  Created by Rashit Osmonov on 27/4/22.
//
import UIKit
import SnapKit

class MainView: UIViewController {
    
    private lazy var tableviewBook: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.register(LentaCell.self, forCellReuseIdentifier: "LentaCell")
        return view
    }()
    private lazy var addButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(systemName: "plus"), for: .normal)
        view.addTarget(self, action: #selector(addClick(view:)), for: .touchUpInside)
        return view
    }()
    @objc func addClick(view: UIButton) {
        navigationController?.pushViewController(AddBookView(), animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(tableviewBook)
        tableviewBook.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
        view.addSubview(addButton)
        addButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.equalToSuperview().offset(-30)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        tableviewBook.reloadData()
    }
}
extension MainView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LocalStore.shared.books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LentaCell", for: indexPath) as! LentaCell
                let model =  LocalStore.shared.books[indexPath.row]
                cell.fill(model: model)
                return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LentaCell", for: indexPath) as! LentaCell
//        let model =  LocalStore.shared.books[indexPath.row]
//        cell.fill(model: model)
//        return cell
//    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return LocalStore.shared.books.count
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 100, height: 100 )
//    }
    }
    

