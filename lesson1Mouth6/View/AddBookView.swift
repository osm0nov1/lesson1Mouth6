//
//  addBook.swift
//  lesson1Mouth6
//
//  Created by Rashit Osmonov on 10/5/22.
//

import Foundation
import SnapKit
import UIKit

class AddBookView: UIViewController {
    var controller = AddBookController()
    private lazy var boolImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "2")
        return view
    }()
    private lazy var textField: UITextField = {
        let view = UITextField()
        view.layer.cornerRadius = 10
        view.placeholder = "Enter Test"
        view.backgroundColor = .gray
        return view
    }()
    private lazy var slider: UISlider = {
        let view = UISlider()
        view.minimumValue = 1
        view.maximumValue = 5
        view.value = 0
        view.addTarget(self, action: #selector(ratingChanged(view:)), for: .valueChanged)
        return view
    }()
    private lazy var bookCover: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "default")
        
        return view
    }()
    private lazy var bookRatingLabel: UILabel = {
        let view = UILabel()
//        view.text = "5"
        return view
    }()
    private lazy var textFieldCaver: UITextField = {
        let view = UITextField()
        view.layer.cornerRadius = 10
        view.placeholder = "Enter Test"
        view.layer.sublayerTransform = CATransform3DMakeTranslation(0, -60, 0);
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .gray
        return view
    }()
    private lazy var save: UIButton = {
        let view = UIButton()
        view.setTitle("Save", for: .normal)
        view.setTitleColor(.white, for: .normal)
        view.layer.backgroundColor = UIColor(red: 0, green: 0.176, blue: 0.89, alpha: 1).cgColor
        view.layer.cornerRadius = 26
        view.addTarget(self, action: #selector(saveBook(view: )), for: .touchUpInside)
        return view
    }()
    @objc func ratingChanged(view: UISlider) {
           bookRatingLabel.text = "Book Rating: \(Int(view.value))"
       }
    @objc func saveBook(view: UIButton){
        controller.addBooks(model: MainModel(image: textField.text, name: "", star: slider.maximumValue, title: textFieldCaver.text))
        navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(boolImage)
        boolImage.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.left.equalToSuperview().inset(100)
        }
        view.addSubview(bookCover)
        bookCover.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalToSuperview().dividedBy(4)
            make.width.equalToSuperview().dividedBy(3)
            make.top.equalTo(view.safeArea.top).offset(10)
        }
        view.addSubview(textField)
        textField.snp.makeConstraints { make in
            make.top.equalTo(boolImage.snp.bottom).offset(30)
            make.right.left.equalToSuperview().inset(25)
            make.height.equalTo(50)
        }
        view.addSubview(slider)
        slider.snp.makeConstraints { make in
            make.top.equalTo(textField.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.width.equalTo(50)
            make.right.left.equalToSuperview().inset(100)
        }
        view.addSubview(bookRatingLabel)
        bookRatingLabel.snp.makeConstraints { make in
            make.top.equalTo(slider.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
        }
        view.addSubview(textFieldCaver)
        textFieldCaver.snp.makeConstraints { make in
            make.top.equalTo(bookRatingLabel.snp.bottom).offset(20)
            make.right.left.equalToSuperview().inset(50)
            make.width.equalTo(50)
            make.height.equalTo(150)
        }
        view.addSubview(save)
        save.snp.makeConstraints { make in
            make.top.equalTo(textFieldCaver.snp.bottom).offset(100)
            make.centerX.equalToSuperview()
            make.width.equalTo(100)
            make.height.equalTo(50)
        }
    }
}

