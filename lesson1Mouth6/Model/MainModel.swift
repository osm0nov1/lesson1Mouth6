//
//  ModelController.swift
//  lesson1Mouth6
//
//  Created by Rashit Osmonov on 6/5/22.
//

import Foundation

struct MainModel{
    var image: String? = nil
    var name: String? = nil
    var star: Float? = nil
    var title: String? = nil
}
